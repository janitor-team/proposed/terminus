# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the terminus package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: terminus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-12 21:18+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/palete.vala:107 src/palete.vala:158
#, c-format
msgid "Error: palette file %s has unrecognized content at line %d\n"
msgstr ""

#: src/palete.vala:120
#, c-format
msgid ""
"Error: palette file %s has opens a bracket at line %d without closing it\n"
msgstr ""

#: src/palete.vala:133
#, c-format
msgid "Error: palette file %s has an unrecognized color at line %d\n"
msgstr ""

#: src/palete.vala:143
#, c-format
msgid "Warning: palette file %s has more than 16 colors\n"
msgstr ""

#: src/palete.vala:168
#, c-format
msgid "Error: Palette file %s has less than 16 colors\n"
msgstr ""

#: src/palete.vala:172
#, c-format
msgid "Error: Palette file %s has no palette name\n"
msgstr ""

#: src/palete.vala:176
#, c-format
msgid "Error: Palette file %s has text_fg color but not text_bg color\n"
msgstr ""

#: src/palete.vala:180
#, c-format
msgid "Error: Palette file %s has text_bg color but not text_fg color\n"
msgstr ""

#: src/params.vala:116
#, c-format
msgid ""
"Parameter '%s' unknown.\n"
"\n"
msgstr ""

#: src/params.vala:123
#, c-format
msgid ""
"The '%s' parameter requires a path after it.\n"
"\n"
msgstr ""

#: src/params.vala:129
#, c-format
msgid ""
"The '%s' parameter requires a command after it.\n"
"\n"
msgstr ""

#: src/params.vala:136
msgid ""
"Usage:\n"
"terminus [OPTION...] [-- COMMAND ...]\n"
"\n"
"Help commands:\n"
"  -h, --help                    show this help\n"
"  -v, --version                 show version\n"
"\n"
"Options:\n"
"  -x, --execute, --             launches a new Terminus window and execute "
"the remainder of the command line inside the terminal\n"
"  -e, --command=STRING          launches a new Terminus window and execute "
"the argument inside the terminal\n"
"  --working-directory=DIRNAME   sets the terminal directory to DIRNAME\n"
"  --no-window                   launch Terminus but don't open a window\n"
"  --nobindkey                   don't try to bind the Quake-mode key (useful "
"for gnome shell)\n"
"  --check-gnome                 exit if we are running it in Gnome Shell "
"(guake mode should be managed by the extension)\n"
msgstr ""

#: src/settings.vala:70
#, c-format
msgid "Version %s"
msgstr ""

#: src/settings.vala:315 src/terminal.vala:118
msgid "New window"
msgstr ""

#: src/settings.vala:316 src/terminal.vala:113
msgid "New tab"
msgstr ""

#: src/settings.vala:317
msgid "Next tab"
msgstr ""

#: src/settings.vala:318
msgid "Previous tab"
msgstr ""

#: src/settings.vala:319
msgid "Show guake terminal"
msgstr ""

#: src/settings.vala:320
msgid "Copy text into the clipboard"
msgstr ""

#: src/settings.vala:321
msgid "Paste text from the clipboard"
msgstr ""

#: src/settings.vala:322
msgid "Move focus to the terminal on the left"
msgstr ""

#: src/settings.vala:323
msgid "Move focus to the terminal on the right"
msgstr ""

#: src/settings.vala:324
msgid "Move focus to the terminal above"
msgstr ""

#: src/settings.vala:325
msgid "Move focus to the terminal below"
msgstr ""

#: src/settings.vala:326
msgid "Make font bigger"
msgstr ""

#: src/settings.vala:327
msgid "Make font smaller"
msgstr ""

#: src/settings.vala:328
msgid "Reset font size"
msgstr ""

#: src/settings.vala:329
msgid "Show menu"
msgstr ""

#: src/settings.vala:336
msgid "Action"
msgstr ""

#: src/settings.vala:337
msgid "Key"
msgstr ""

#: src/terminal.vala:92
msgid "Copy"
msgstr ""

#: src/terminal.vala:97
msgid "Paste"
msgstr ""

#: src/terminal.vala:104
msgid "Split horizontally"
msgstr ""

#: src/terminal.vala:108
msgid "Split vertically"
msgstr ""

#: src/terminal.vala:125
msgid "Preferences"
msgstr ""

#: src/terminal.vala:132
msgid "Close"
msgstr ""

#: src/terminus.vala:60
#, c-format
msgid "Version %s\n"
msgstr ""

#: src/terminus.vala:93
msgid "Custom colors"
msgstr ""

#: data/interface/properties.ui:22
msgid "Block"
msgstr ""

#: data/interface/properties.ui:26
msgid "Double T"
msgstr ""

#: data/interface/properties.ui:30
msgid "Underscore"
msgstr ""

#: data/interface/properties.ui:80
msgid "Enable guake mode"
msgstr ""

#: data/interface/properties.ui:99
msgid "Cursor shape:"
msgstr ""

#: data/interface/properties.ui:128
msgid "Terminal bell"
msgstr ""

#: data/interface/properties.ui:144
msgid "Custom font:"
msgstr ""

#: data/interface/properties.ui:161
msgid "Custom command shell:"
msgstr ""

#: data/interface/properties.ui:180
msgid "Use custom shell"
msgstr ""

#: data/interface/properties.ui:200
msgid "General"
msgstr ""

#: data/interface/properties.ui:221 data/interface/properties.ui:477
msgid "Built-in schemes:"
msgstr ""

#: data/interface/properties.ui:276
msgid "Text"
msgstr ""

#: data/interface/properties.ui:288
msgid "Background"
msgstr ""

#: data/interface/properties.ui:312
msgid "Default color:"
msgstr ""

#: data/interface/properties.ui:321
msgid "Bold color:"
msgstr ""

#: data/interface/properties.ui:335
msgid "Cursor color:"
msgstr ""

#: data/interface/properties.ui:349
msgid "Highlight color:"
msgstr ""

#: data/interface/properties.ui:426
msgid "<b>Text and background colors</b>"
msgstr ""

#: data/interface/properties.ui:440
msgid "<b>Palette</b>"
msgstr ""

#: data/interface/properties.ui:509
msgid "Colors palete:"
msgstr ""

#: data/interface/properties.ui:782
msgid "Colors"
msgstr ""

#: data/interface/properties.ui:808
msgid "Scrollback lines:"
msgstr ""

#: data/interface/properties.ui:830
msgid "0"
msgstr ""

#: data/interface/properties.ui:840
msgid "Infinite Scrollback"
msgstr ""

#: data/interface/properties.ui:887
msgid "Scroll on output"
msgstr ""

#: data/interface/properties.ui:901
msgid "Scroll on keystroke"
msgstr ""

#: data/interface/properties.ui:922
msgid "Scrolling"
msgstr ""

#: data/interface/properties.ui:950
msgid "Keybindings"
msgstr ""

#: data/interface/properties.ui:970
msgid "Terminus"
msgstr ""

#: data/interface/properties.ui:993
msgid ""
"A tiled terminal emulator.\n"
"\n"
"Written by Sergio Costas Rodríguez (rastersoft)\n"
"\n"
"http://www.rastersoft.com\n"
"rastersoft@gmail.com"
msgstr ""

#: data/interface/properties.ui:1016
msgid "About"
msgstr ""
